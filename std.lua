--[[
  std.lua

    >>> Lua Standard Library for OpenCV Scripts <<<

  (c) 2018 Bifozp
]]

local m = {}

---------------------------------------
-- Internal ---------------------------
---------------------------------------

-- Number型か否か
local function _isNumber(obj)
  return 'number' == type(obj)
end


-- String型か否か
local function _isString(obj)
  return 'string' == type(obj)
end


-- Boolean型か否か
local function _isBool(obj)
  return 'boolean' == type(obj)
end


-- Stringの中身が空か否か
local function _isEmptyString(obj)
  if _isString(obj) then
    if '' == obj then
      return true
    end
  end
  return false
end


-- ファイルが読み込み可能か否か
local function _freadable(path)
  local f = io.open(path,"r")
  if f == nil then
    return false
  end
  io.close(f)
  return true
end


-- スクリプトのあるディレクトリの取得
--   引数はコールレベル
local function _getScriptDir(lev)
  local path = debug.getinfo(lev, "S").source:sub(2)
  local s = path:match("([^\r\n]*[/\\])")
  if _isString(s) then
    return s
  end
  return nil
end


---------------------------------------
-- Global -----------------------------
---------------------------------------

-- 引数で与えられたオブジェクトが数値か否かを返す
m.isNumber = _isNumber


-- 引数で与えられたオブジェクトが文字列か否かを返す
m.isString = _isString


-- 引数で与えられたオブジェクトがboolean型か否かを返す
m.isBool = _isBool


-- 引数で与えられたオブジェクトが空の文字列か否かを返す
m.isEmptyString = _isEmptyString


-- 引数で与えられたオブジェクトがNilか、空の文字列か否かを返す
-- 数値・テーブル・関数型は(空文字列としては)空と判断する
m.isNilOrEmptyString = function(obj)
  if not ('string' == type(obj)) then   -- nil : nil
    return true                         -- table / number : empty string
  end
  return _isEmptyString(obj)
end


-- 引数srcの数値を取得する
--   string型の場合は数値に変換を試みた結果を返す
--   boolean型の場合は、true=1,false=0 を返す
--   srcからの取得に失敗した場合、defaultの値を返す
--   defaultの型が数値型でなかった場合は、nilを返す
m.getNumberSecure = function(src, default)
  if _isNumber(src) then
    return src
  end
  if _isString(src) then
    local num = tonumber(src)
    if _isNumber(num) then
      return num
    end
  end
  if _isBool(src) then
    if src then
      return 1
    end
    return 0
  end
  if _isNumber(default) then
    return default
  end
  return nil
end


-- 引数srcの真偽値を取得する
--   string型の場合は数値に変換を試みた結果を返す
--   number型の場合は、0=false,0以外=1 を返す
--   srcからの取得に失敗した場合、defaultの値を返す
--   defaultの型がboolean型でなかった場合は、nilを返す
m.getBoolSecure = function(src, default)
  if _isBool(src) then
    return src
  end
  if _isString(src) then
    if 'true' == src then
      return true
    elseif 'false' == src then
      return false
    end
  end
  if _isNumber(src) then
    if 0 ~= src then
      return true
    end
    return false
  end
  if _isBool(default) then
    return default
  end
  return nil
end


-- 引数srcの文字列を取得する
--   number型/boolean型の場合は文字列に変換を試みた結果を返す
--   srcからの取得に失敗した場合、defaultの値を返す
--   defaultの型がstring型でなかった場合は、nilを返す
m.getStringSecure = function(dst, src, default)
  if _isString(src) then
    return src
  end
  if _isNumber(src) or _isBool(src) then
    return tostring(src)
  end
  if _isString(default) then
    return default
  end
  return nil
end


-- ファイルが読み込み可能か否かを返す
--   実行中のスクリプトやDLLのファイルパスを与えてこの関数を実行すると、
--   たいてい失敗するので注意。
m.freadable = _freadable


-- コール元スクリプト自身のあるディレクトリパスを返す
--   AviUtlの各種スクリプトファイルから実行しても、正しいパスは得られない(nilを返す)。
--   requireやdofile先で使用するのがよいかと。
m.getScriptDir = function()
  return _getScriptDir(2)
end


-- std.lua のあるディレクトリパスを返す
m.getStdScriptDir = function()
  return _getScriptDir(1)
end


return m
