--[[
  coords.lua

  (c) 2018 Bifozp

  座標系変換モジュール
    ※ ろくに動作確認していません
    
    <入力>
      - x (number) X座標
      - y (number) Y座標
      - w (number) フレーム幅 (※)
      - h (number) フレーム高さ (※)

      (※) オブジェクト幅および高さではないので注意
]]

local m = {}

-- 中心基準相対座標(AviUtl系座標) → 左上基準絶対座標(他モジュール操作系座標)
m.RelativeToAbsolute = function(x,y,w,h)
  local hw = w/2
  local hh = h/2
  
  local ax = hw + x
  local ay = hh + y
  return ax, ay
end


-- 左上基準絶対座標(他モジュール操作系座標) → 中心基準相対座標(AviUtl系座標)
m.AbsoluteToRelative = function(x,y,w,h)
  local hw = w/2
  local hh = h/2

  local rx = x - hw
  local ry = y - hh
  return rx, ry
end

return m
