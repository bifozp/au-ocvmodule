--[[
  bbproc.lua

  (c) 2018 Bifozp

  動体検出 / BB化 共通処理モジュール
]]

local std = require('std')

local m = {}

m.setMogParams = function(param, useParams)
  param.method = 1  -- MOG
  param.useParams = false
  if 0 ~= useParams then
    param.useParams       = true
    param.history         = std.getNumberSecure(history, 200)
    param.nmixtunes       = std.getNumberSecure(nmixtunes, 5)
    param.backgroundRatio = std.getNumberSecure(backgroundRatio, 0.7)
    param.noiseSigma      = std.getNumberSecure(noiseSigma, 0)
  end
end

m.setMog2Params = function(param, useParams)
  param.method = 2  -- MOG2
  param.useParams = false
  if 0 ~= useParams then
    param.useParams       = true
    param.history         = std.getNumberSecure(history, 500)
    param.varThreshold    = std.getNumberSecure(varThreshold, 16)
    param.detectShadows   = std.getBoolSecure(detectShadows, true)
  end
end

m.setGmgParams = function(param, useParams)
  param.method = 3  -- GMG
  param.useParams = false
  if 0 ~= useParams then
    param.useParams            = true
    param.initializationFrames = std.getNumberSecure(initializationFrames, 120)
    param.detectionThreshold   = std.getNumberSecure(detectionThreshold, 0.8)
  end
end


m.setKnnParams = function(param, useParams)
  param.method = 4  -- KNN
  param.useParams = false
  if 0 ~= useParams then
    param.useParams       = true
    param.history         = std.getNumberSecure(history, 500)
    param.dist2Threshold  = std.getNumberSecure(dist2Threshold, 400)
    param.detectShadows   = std.getBoolSecure(detectShadows, true)
  end
end


m.setCntParams = function(param, useParams)
  param.method = 5  -- CNT
  param.useParams = false
  if 0 ~= useParams then
    param.useParams         = true
    param.minPixelStability = std.getNumberSecure(minPixelStability, 15)
    param.useHistory        = std.getBoolSecure(useHistory, true)
    param.maxPixelStability = std.getNumberSecure(maxPixelStability, 15*60)
    param.isParallel        = std.getBoolSecure(isParallel, true)
  end
end


m.setLsbpParams = function(param, useParams)
  param.method = 6  -- LSBP
  param.useParams = false
  if 0 ~= useParams then
    param.useParams                  = true
    param.mc                         = std.getNumberSecure(mc, 0)
    param.nSamples                   = std.getNumberSecure(nSamples, 20)
    param.LSBPRadius                 = std.getNumberSecure(LSBPRadius, 16)
    param.Tlower                     = std.getNumberSecure(Tlower, 2)
    param.Tupper                     = std.getNumberSecure(Tupper, 32)
    param.Tinc                       = std.getNumberSecure(Tinc, 1)
    param.Tdec                       = std.getNumberSecure(Tdec, 0.05)
    param.Rscale                     = std.getNumberSecure(Rscale, 10)
    param.Rincdec                    = std.getNumberSecure(Rincdec, 0.005)
    param.noiseRemovalThresholdFacBG = std.getNumberSecure(noiseRemovalThresholdFacBG, 0.0004)
    param.noiseRemovalThresholdFacFG = std.getNumberSecure(noiseRemovalThresholdFacFG, 0.0008)
    param.LSBPthreshold              = std.getNumberSecure(LSBPthreshold, 8)
    param.minCount                   = std.getNumberSecure(minCount, 2)
  end
end


m.setGsocParams = function(param, useParams)
  param.method = 7  -- GSOC
  param.useParams = false
  if 0 ~= useParams then
    param.useParams                    = true
    param.mc                           = std.getNumberSecure(mc, 0)
    param.nSamples                     = std.getNumberSecure(nSamples, 20)
    param.replaceRate                  = std.getNumberSecure(replaceRate, 0.003)
    param.propagationRate              = std.getNumberSecure(propagationRate, 0.01)
    param.hitsThreshold                = std.getNumberSecure(hitsThreshold, 32)
    param.alpha                        = std.getNumberSecure(alpha, 0.01)
    param.beta                         = std.getNumberSecure(beta, 0.0022)
    param.blinkingSupressionDecay      = std.getNumberSecure(blinkingSupressionDecay, 0.1)
    param.blinkingSupressionMultiplier = std.getNumberSecure(blinkingSupressionMultiplier, 0.1)
    param.noiseRemovalThresholdFacBG   = std.getNumberSecure(noiseRemovalThresholdFacBG, 0.0004)
    param.noiseRemovalThresholdFacFG   = std.getNumberSecure(noiseRemovalThresholdFacFG, 0.0008)
  end
end


m.setBGSubParams = function(proc, param, useBelow)
  if 0 ~= obj.frame then
    param.method = 0
    return
  end
  proc(param, useBelow)
end


m.exec_pf = function(loadtype, loadparam, inv, bgcolor, transmit, param)

  -- 退避
  obj.copybuffer("tmp", "obj")

  -- 差分オブジェクト読み出し
  obj.load(loadtype, loadparam)
  local kud, kfw, kfh = obj.getpixeldata("alloc")
  
  -- 復元
  obj.load("tempbuffer")
  -- 現フレームの読み出し
  local ud, fw, fh = obj.getpixeldata()

  -- オブジェクトサイズのチェック
  if( (kfw ~= fw) or (kfh ~= fh) ) then
    debug_print('Err: bbproc.exec(): Object size mismatch.')
    return
  end

  -- 処理実行
  if ocvmodule.bg_subtract_pf(ud, kud, fw, fh, inv, bgcolor, transmit, param) then
    -- debug_print("succeeded")
    obj.putpixeldata(ud)
  end
end


m.exec = function(inv, bgcolor, bgmode, ipr, ipm, param)
  local ud, fw, fh = obj.getpixeldata()
  if ocvmodule.bg_subtract(ud, fw, fh, inv, bgmode, bgcolor, ipr, ipm, param) then
    -- debug_print("succeeded")
    obj.putpixeldata(ud)
  end
end

return m
