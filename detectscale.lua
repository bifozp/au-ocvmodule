--[[
  detectscale.lua

  (c) 2018 Bifozp

  フェイススタンプ系効果の共通処理モジュール
]]

local m = {}

--[[
  顔にスタンプを貼る
]]
m.faceStamp = function(coords, std, xmlPath)
  -- パラメータ取得/チェック
  local reducr = obj.track0            -- 縮小倍率
  local gray   = 0                     -- グレースケール化有無
  if obj.check0 then gray=1 end
  local sf = obj.track1                -- ScaleFactor: 換算係数
  local mn = obj.track2                -- minNeighghbors: 最小近傍数
  local ro = obj.track3                -- rotate: 回転速度

  -- 顔に貼るスタンプイメージファイルのパスチェック
  if nil == file or 'string' ~= type(file) or file == '' then
    debug_print("Error: no input.")
    return
  elseif not std.freadable(file) then
    --debug_print("Error: image file is not exist.")
    return
  end
  -- 学習データxmlのパスチェック
  if not std.freadable(xmlPath()) then
    debug_print("Error: xml file is not exist.")
    return
  end

  -- 作業データ作成
  obj.load("framebuffer")
  local ud,fw,fh = obj.getpixeldata()

  -- OpenCVの顔検出処理を呼び出す
  local faces = ocvmodule.detect_faces(ud,fw,fh, xmlPath(), gray,reducr, sf,mn, 0)
  if nil == faces then
    -- debug_print("faces is nothing")
    obj.draw(0,0,0,0) -- 読み出したframebufferを描画しない為の空draw
    return
  end

  -- 画像読み込み/エフェクトの適用
  obj.load("image", file)
  obj.effect()

  -- 検出座標毎の処理
  for k,v in pairs(faces) do
    -- 処理座標の取得
    local ox,oy = coords.AbsoluteToRelative(v.x, v.y, fw, fh)
    ox = ox + (v.width/2)
    oy = oy + (v.height/2)

    -- 画像の読み込み・貼り付け
    local fmax = v.width
    if v.height > fmax then fmax = v.height end
    local imin = obj.w
    if obj.h < imin then imin = obj.h end
    obj.draw(ox,oy,0,(fmax/imin)*(obj.getvalue("zoom")/100),1,0,0,obj.time*ro)
  end
end


return m
